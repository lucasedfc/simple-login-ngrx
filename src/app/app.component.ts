import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BooksService } from './books/service/books.service';
import { UsersService } from './users/service/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  logged$: Observable<boolean>;
  username$: Observable<string>;
  photoUrl$: Observable<string>;
  books$: Observable<any[]>;
  title = 'ngrx-example';

  constructor(
    private userService: UsersService,
    private booksService: BooksService
  ) {
    this.logged$ = this.userService.isLogged$();
    this.username$ = this.userService.getUsername$();
    this.photoUrl$ = this.userService.getPhotoUrl$();
    this.books$ = this.booksService.getBooks$();
  }

  ngOnInit(): void {
    this.loadBooks()
  }

  login(username: string) {
    console.log('login method');

    this.userService.doLoginProcess(username);
  }

  logout() {
    console.log('logout method');

    this.userService.doLogoutProcess();
  }

  loadBooks() {
    this.booksService.loadBooksByCurrentUser();
  }
}
