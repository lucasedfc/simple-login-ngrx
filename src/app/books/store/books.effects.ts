import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, exhaustMap, map, of, switchMap, take } from "rxjs";
import { ApiService } from "src/app/api/service/api.service";
import { BooksActions } from "src/app/books/store/books.actions";
import { UsersSelectors } from "src/app/users/store/users.selectors";


@Injectable()

export class BooksEffects {
  constructor(
    private actions$: Actions, private apiService: ApiService, private store: Store) { }

  loadBooksByCurrentUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BooksActions.LOAD_BOOKS_FOR_CURRENT_USER_ACTION_TYPE),
      exhaustMap((action, state) => {
        console.log('BooksEffects => Listeintg actions$: ofType(BooksActions.LOAD_BOOKS_FOR_CURRENT_USER_ACTION_TYPE)');
        return of(true).pipe(
          switchMap((action, state) => {
            return this.store.select(UsersSelectors.getUsername);
          }),
          take(1),
          switchMap((username: string) => {
            return this.apiService.getBooksByUserName(username)
          }),
          map((result: any[]) => {
            console.log('BooksEffects => dispatch BooksActions.loadListSuccess({result})');
            return BooksActions.loadListSuccess({ result });
          }),
          catchError((error) => of(BooksActions.loadListError({ error })))
        )
      })
    ))

  removeBooksByCurrentUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BooksActions.REMOVE_BOOKS_FOR_CURRENT_USER_ACTION_TYPE),
      exhaustMap((action, state) => {
        console.log('BooksEffects => Listeintg actions$: ofType(BooksActions.REMOVE_BOOKS_FOR_CURRENT_USER_ACTION_TYPE)');
        return of(true).pipe(
          switchMap((action, state) => {
            return this.store.select(UsersSelectors.getUsername);
          }),
          take(1),
          switchMap((username: string) => {
            return of([]);
          }),
          map((result: any[]) => {
            console.log('BooksEffects => dispatch BooksActions.loadListSuccess({result})');
            return BooksActions.loadListSuccess({ result });
          }),
          catchError((error) => of(BooksActions.loadListError({ error })))
        )
      })
    ))






}
