import { Action, ActionReducer, createReducer, on } from "@ngrx/store";
import { BooksActions } from "./books.actions";
import { BooksState } from "./books.state";


export const initialUserState: BooksState = {
  list: []
}

export class BooksReducers {
  private static readonly _reducersDefs: ActionReducer<BooksState, Action> =
    createReducer(
      initialUserState,

      on(BooksActions.loadListSuccess, (state, {result}) => {
        console.log('BooksActions => BooksActions.loadListSuccedd', result);
        return {
          ...state,
          list: result
        }
      }),

      on(BooksActions.loadListError, (state, {error}) => {
        console.log('BooksActions => BooksActions.loadListError', error);
        return {
          ...state,
          list: []
        }
      }),

    ) as ActionReducer<BooksState, Action>;

    public static getReducers(state: any, action: any) {
      return BooksReducers._reducersDefs(state, action);
    }
}
