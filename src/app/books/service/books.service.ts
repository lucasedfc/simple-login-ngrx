import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { catchError, map, Observable, of, switchMap, take } from "rxjs";
import { ApiService } from "src/app/api/service/api.service";
import { UsersSelectors } from "src/app/users/store/users.selectors";
import { BooksActions } from "../store/books.actions";
import { BooksSelectors } from "../store/books.selectors";
import { BooksState } from "../store/books.state";


@Injectable()
export class BooksService {
  constructor(
    private store: Store<{state: BooksState}>,
    private apiService: ApiService
  ){}

  public getBooks$(): Observable<any[]> {
    return this.store.select(BooksSelectors.getBooks);
  }


  public loadBooksByCurrentUser() {
    console.log('BooksService => loadBooksByCurrentUser');

    this.store.select(UsersSelectors.getUsername)
      .pipe(
        take(1),
        switchMap((username: string) => {
          console.log('BooksService => this.apiService.getBooksByCurretUser(username)',
          username
          );
          return this.apiService.getBooksByUserName(username);
        }),
        map((result: any[]) => {
          console.log(
            'BooksService => dispatch actions$: BooksActions.loadListSuccess({result})',
            result
          );
          return this.store.dispatch(BooksActions.loadListSuccess({result}))
        }),
        catchError((error) => {
          this.store.dispatch(BooksActions.loadListError({error}))
          return of(error)
        })
      ).subscribe(() => {})

  }

}
