import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { ApiService } from "src/app/api/service/api.service";
import { UserModel } from "../model/user.model";
import { UsersActions } from "../store/users.actions";
import { UsersSelectors } from "../store/users.selectors";
import { UsersState } from "../store/users.state";

@Injectable()
export class UsersService {
  constructor(
    private store: Store<{state: UsersState}>,
    private apiService: ApiService
  ){}

  public isLogged$(): Observable<boolean> {
    return this.store.select(UsersSelectors.isLogged);
  }

  public getUsername$(): Observable<string> {
    return this.store.select(UsersSelectors.getUsername);
  }

  public getPhotoUrl$(): Observable<string> {
    return this.store.select(UsersSelectors.getPhotoUrl);
  }

  public doLoginProcess(username: string) {
    console.log('UsersService => doLoginProcess');
    //TODO: process to login
    this.apiService.getUserData(username)
      .subscribe((userResult: UserModel) => {
        this.store.dispatch(UsersActions.loginSuccess({userData: userResult}));
      })
  }

  public doLogoutProcess() {
    console.log('UsersService => doLogoutProcess');
    //TODO: process to logout
    this.apiService.clearUserData()
      .subscribe((result: boolean) => {
        this.store.dispatch(UsersActions.logoutSuccess());
      })
  }
}
