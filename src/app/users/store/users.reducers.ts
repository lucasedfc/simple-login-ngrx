import { Action, ActionReducer, createReducer, on } from "@ngrx/store";
import { UsersActions } from "./users.actions";
import { UsersState } from "./users.state";

export const initialUserState: UsersState = {
  logged: false,
  currentUser: null
}

export class UsersReducers {
  private static readonly _reducersDefs: ActionReducer<UsersState, Action> =
    createReducer(
      initialUserState,

      on(UsersActions.loginSuccess, (state, action) => {
        console.log('UsersActions => UsersActions.login');
        return {
          ...state,
          logged: true,
          currentUser: action.userData
        }
      }),

      on(UsersActions.logoutSuccess, (state) => {
        console.log('UsersActions => UsersActions.logout');
        return {
          ...state,
          logged: false,
          currentUser: null
        }
      }),

    ) as ActionReducer<UsersState, Action>;

    public static getReducers(state: any, action: any) {
      return UsersReducers._reducersDefs(state, action);
    }
}
