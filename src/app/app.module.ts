import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducer, ActionReducerMap, MetaReducer, StoreModule } from '@ngrx/store';
import { UsersService } from './users/service/users.service';
import { UsersState } from './users/store/users.state';
import { UsersReducers } from './users/store/users.reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ApiService } from './api/service/api.service';
import { HttpClientModule } from "@angular/common/http";
import { BooksState } from './books/store/books.state';
import { BooksReducers } from './books/store/books.reducers';
import { BooksService } from './books/service/books.service';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './users/store/users.effects';
import { BooksEffects } from './books/store/books.effects';
import { localStorageSync } from 'ngrx-store-localstorage';

interface AppState {
  users: UsersState,
  books: BooksState
}

const appReducers: ActionReducerMap<AppState> = {
  users: UsersReducers.getReducers,
  books: BooksReducers.getReducers
};

const appEffects: any[] = [UsersEffects, BooksEffects]

export function localStorageSyncReducer(
  reducer: ActionReducer<AppState>
): ActionReducer<AppState> {
  return localStorageSync({
    keys: [
      'users',
    ],
    rehydrate: true,
    storageKeySerializer: (key) => `app_${key}`,
  })(reducer)
}

const customMetaReducers: Array<MetaReducer<any, any>> = [
  localStorageSyncReducer
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers, {
      metaReducers: customMetaReducers
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot(appEffects)
  ],
  providers: [UsersService, ApiService, BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
