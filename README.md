# NgrxSimpleLogin

It is a simple login application with the objective of implementing NGRX (REDUX) to manage the state of the user and books (as an example to store more data to the store)
When logging in (admin or user) an image corresponding to the user in question is loaded and a request is made to an API to consult a random number of books.
Set ngrx-store-localstorage to handle state on site reload.

## Dependencies

Clone the projects and run `npm i` to install dependencies.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.


To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
